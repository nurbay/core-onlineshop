FROM darecosystem/java8-deb

MAINTAINER //TODO YOUR NAME AND EMAIL

ENV APP_NAME core_onlineshop-1.0.0.zip
ENV APP_DIR core_onlineshop-1.0.0
ENV JAVA_OPTS -Xms128M -Xmx512M -Xss1M -XX:+CMSClassUnloadingEnabled
ENV RUN_SCRIPT boot
ENV LANG C.UTF-8

# logs
RUN mkdir -p /root/config/

COPY ./src/main/resources/*-.conf /root/config/

WORKDIR /root
COPY ./target/universal/$APP_NAME /root/
RUN unzip -q $APP_NAME
WORKDIR /root/$APP_DIR/bin
CMD chmod +x $RUN_SCRIPT
#EXPOSE 7770
CMD ./$RUN_SCRIPT