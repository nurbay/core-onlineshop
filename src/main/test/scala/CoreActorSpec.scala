
import java.time.Duration

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.github.sstone.amqp.RpcClient
import kz.darlab.akka.core.{CoreActor, ElasticsearchDAO}
import kz.darlab.akka.domain.amqp.Request
import kz.darlab.akka.domain.entities.DTO_.DtoProduct
import kz.darlab.akka.domain.entities.DomainEntity
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import org.scalatest.mock.MockitoSugar
import org.scalatest.mock._
import org.specs2.mock.Mockito

import scala.concurrent.duration._
import scala.concurrent.duration.FiniteDuration

class CoreActorSpec (_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll  with Mockito{

  def this() = this(ActorSystem("CoreActorSystem"))

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }


  val publisherActor = TestProbe().ref
  val elasticsearchDAO = mock[ElasticsearchDAO]
  elasticsearchDAO.addProduct(any)  returns true


  val coreActor = system.actorOf(CoreActor.props(publisherActor, elasticsearchDAO))

  "CoreActor " must {
    "must add some product" in {

      coreActor ! Request(Some("response.AddProduct"), Map(), DtoProduct(8, "Meizu 16", Some("Meizu Co"), Some("Meizu 16 2018")), "request.readPOST.addproduct")

    expectMsg(5000 millis,  "true")
    }
  }

}
