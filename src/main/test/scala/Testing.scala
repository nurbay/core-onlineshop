import com.sksamuel.elastic4s.http._
import kz.darlab.akka.core.{CoreActorTest, ElasticsearchDAO}
import kz.darlab.akka.domain.entities.DAO_.Product
import org.specs2.mutable.Specification
import com.sksamuel.elastic4s.http.search.{SearchHit, SearchHits, SearchResponse}
import kz.darlab.akka.domain.entities.DTO_.DtoProduct
import org.specs2.mock._
import scala.concurrent.Future


class Testing extends Specification with Mockito {

  import scala.concurrent.ExecutionContext.Implicits.global

  var elasticDao = mock[ElasticsearchDAOTest].verbose

  elasticDao.getProducts returns Future {
    Right(RequestSuccess(200, None, Map(), SearchResponse(5, false, false, null,
      Shards(5, 0, 5), None, null, SearchHits(total = 1, maxScore = 2.0,
            hits = Array(SearchHit("re7qZ2UBtDTxyozGMiJM", "products", "doc26",
        1.0f, None, None, Map("id" -> "12", "category_id" -> "2","name" -> "Samsung","supplier" -> "Samsung Co","description" -> "J5 15"), null, null, null, 1),
        SearchHit("r-7tZ2UBtDTxyozG8SK5", "products", "doc26", 1.0f, None, None, Map("id" -> "12",
          "category_id" -> "2",
            "name" -> "Samsung",
            "supplier" -> "Samsung Co",
            "description" -> "J5 15"
          )
          , null, null, null, 1)
      )))))
  }

  elasticDao.updateProduct(any) returns Future { Left (RequestFailure(404,Some("""{"error":{"root_cause":[{"type":"index_not_found_exception","reason":"no such index","resource.type":"index_or_alias","resource.id":"products3453","index_uuid":"_na_","index":"products3453"}],"type":"index_not_found_exception","reason":"no such index","resource.type":"index_or_alias","resource.id":"products3453","index_uuid":"_na_","index":"products3453"},"status":404}""")
            ,Map(),
            ElasticError("index_not_found_exception","no such index",Some("_na_"),Some("products3453"),None,List(ElasticError("index_not_found_exception","no such index",Some("_na_"),Some("products3453"),None,null)))
  ) )}

  var coreActor = new CoreActorTest(elasticDao)
  var res = List[Product]()
  coreActor.getProdList.map(s => res = s)

  "CoreActor getProducts function" >> {
      res must be equalTo List(Product(12,2,"Samsung",Some("Samsung Co"),Some("J5 15")), Product(12,2,"Samsung",Some("Samsung Co"),Some("J5 15")))
    }

  var doUpdate = 0
  val dtoProd = DtoProduct(1,"Phone",Some("suplier name"),Some("description"))
  coreActor.updateProduct(dtoProd).map(s => doUpdate = s)
  "CoreActor updateProduct function" >> {
    "in case of failure" >> {
      doUpdate must be equalTo(1)
    }

  }











}
