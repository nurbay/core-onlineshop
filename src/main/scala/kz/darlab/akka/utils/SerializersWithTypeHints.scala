package kz.darlab.akka.utils



import kz.darlab.akka.domain.entities.DomainEntity
import kz.darlab.akka.domain.serializers.{CoreSerializer, DateTimeSerializer, OnlineShopSerializer}
import org.json4s.FieldSerializer
//import com.fasterxml.jackson.datatype.joda.ser.DateTimeSerializer
import org.json4s.{NoTypeHints, ShortTypeHints}
import org.json4s.native.Serialization


trait SerializersWithTypeHints extends DateTimeSerializer  with CoreSerializer with OnlineShopSerializer {

  val dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

  implicit val formats = Serialization.formats(ShortTypeHints(coreTypeHints ++ onlineShopTypeHints)) +
    new DateTimeSerializer() + FieldSerializer[DomainEntity]()


}

