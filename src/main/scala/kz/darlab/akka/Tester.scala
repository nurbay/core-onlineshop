package kz.darlab.akka

import kz.darlab.akka.core.Products.{nextId, productList}
import kz.darlab.akka.domain.entities.DAO_.{Product, UserState, WebUser}
import kz.darlab.akka.domain.entities.DTO_.DtoProduct

object Tester  {

case class Pojo (id:Int,name:String)

  var a = List (1,2,3,List(77,99))
  val b = List (5)
   a ++=b
  println(a)

  var pojoList:List[Pojo] = List ( Pojo(18,"first"),Pojo(2,"second") ,Pojo(3,"third"))

   println( pojoList(0).id )

  val x=0
  var y= -1
  pojoList.map (e=> if (e.id==x)  y=pojoList.indexOf(e) )

  var pojoList2 :List[Pojo] = Nil

  for (i<- 0 to pojoList.size-1 if i!=y)
  pojoList2 = pojoList(i) :: pojoList2

  println("-P2: "+pojoList2)

  pojoList2 :+= Pojo (2,"second")
  println("-P2: "+pojoList2)

  println(pojoList2(0) == pojoList2(1))

  var s :Either[Int,String]= Right("asdf")

  var productList = List(Product(nextId, 1, "Iphone X", Some("Apple"), Some("iphone x 128GB"))
    , Product(nextId, 1, "Huawei P10", Some("Huawei"), Some("32GB Ram, 12Mp,5.2Inch"))
    , Product(nextId, 2, "Macbook", Some("Apple"), Some("Macbook Air 2015")))

  val dto = DtoProduct( 2, "Macbook", Some("Apple"), Some("Macbook Air 2015"))
  y = -1
  productList.map (e=> if (e.category_id == dto.category_id && e.name.matches(dto.name))
    y = productList.indexOf(e))

  var productList2: List[Product] = Nil

  for (i <- 0 to productList.size - 1 if i != y)  productList2 = productList(i) :: productList2
  productList = productList2.reverse

  println(productList)

  val users:List[WebUser] = List (WebUser(1,"nbay","pasword",UserState.New,"Nursultan Serikovich"),
    WebUser(1,"manager","manager",UserState.Active,"onlineshop Manager")
  ,WebUser(1,"admin","admin",UserState.Active,"Admin user"))

  var check  = false
  val wu = WebUser(1,"nbay","pasword",UserState.New,"Nursultan Serikovich")
  val wuu = WebUser
  users.map (e => if (e.login==wu.login && e.password.matches(wu.password)) check=true   )

  println (check)

  println(wu)
  println(wuu)




}
