package kz.darlab.akka.core

import java.text.SimpleDateFormat
import java.util.TimeZone

import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.{ObjectMapper, SerializationFeature}
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.fasterxml.jackson.datatype.joda.cfg.JacksonJodaDateFormat
import com.fasterxml.jackson.datatype.joda.ser.DateTimeSerializer
import com.sksamuel.elastic4s._
import com.sksamuel.elastic4s.delete.DeleteApi
import com.sksamuel.elastic4s.http.bulk.BulkResponse
import com.sksamuel.elastic4s.http.{HttpClient, RequestFailure, RequestSuccess}
import com.sksamuel.elastic4s.jackson.JacksonSupport
import com.sksamuel.elastic4s.http.delete.DeleteByQueryResponse
import com.sksamuel.elastic4s.http.update.UpdateResponse
import com.sksamuel.elastic4s.update.UpdateApi
import com.typesafe.config.ConfigFactory
import kz.darlab.akka.domain.entities.DAO_.Product
import kz.darlab.akka.domain.entities.DTO_.DtoProduct
import kz.darlab.akka.domain.entities.DomainEntity
import kz.darlab.akka.utils.DarEndpoint
import org.joda.time.format.DateTimeFormat
import org.slf4j.LoggerFactory
import com.sksamuel.elastic4s.json4s.ElasticJson4s

trait PersonIndexes {

  implicit def JacksonJsonIndexable[T](implicit mapper: ObjectMapper = JacksonSupport.mapper): Indexable[T] = {
    new Indexable[T] {
      mapper.registerModule(new JodaModule().addSerializer(new DateTimeSerializer(new JacksonJodaDateFormat(DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")),0)))
      mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
      mapper.setSerializationInclusion(Include.NON_EMPTY)
      mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"))
      mapper.setTimeZone(TimeZone.getTimeZone("Asia/Almaty"))
      override def json(t: T): String = mapper.writeValueAsString(t)
    }
  }

}
object ElasticsearchDAOTest {

  def apply(): ElasticsearchDAO = {
    val config    = ConfigFactory.load()
    val hostname  = config.getString("elasticsearch.hostname")
    val port      = config.getInt("elasticsearch.port")
    new ElasticsearchDAO(HttpClient(ElasticsearchClientUri(hostname, port)))
  }
}

class ElasticsearchDAOTest(client: HttpClient) extends  PersonIndexes with DarEndpoint with DeleteApi with UpdateApi{

  import com.sksamuel.elastic4s.http.ElasticDsl._
  val log = LoggerFactory.getLogger(this.getClass)

  //************************************************************************************************
  //
  def addProduct(p: DomainEntity) = {
    var success = false
    val tmp = p.asInstanceOf[Product]
    println("*************************************************")
    client.execute {
      search("products") .bool(must(matchQuery("category_id", tmp.category_id),matchQuery("name", tmp.name)))
    }.await match {
      case Right(rs@RequestSuccess(status, body, headers, result)) =>
        if (result.hits.total==0)
          client.execute {
            bulk(
              indexInto("products" / "doc26").source(p).refresh(RefreshPolicy.WaitFor)
            )
          }.await match {
            case Right(requestSuccess: RequestSuccess[BulkResponse]) =>
              log.info(s"CreatedEvent implemented $p")
              success = true
            case Left(requestFailure: RequestFailure) =>
              log.info(s"CreatedEvent failed of $p reason: ${requestFailure}")
          }
      case Left(requestFailure: RequestFailure) =>  log.info(s"Search for product failed of  reason: ${requestFailure}")
    }
    success
  }

  def delete(id: Int) = {
    var success = false

    client.execute {
      search("products").matchQuery("id", id)
    }.await match {
      case Right(rs@RequestSuccess(status, body, headers, result)) =>
        client.execute {
          deleteByQuery("products", "doc26", matchQuery("id", id))
        }.await match {
          case Right(requestSuccess: RequestSuccess[DeleteByQueryResponse]) =>
            log.info(s"CreatedEvent implemented $id")
            success = true
          case Left(requestFailure: RequestFailure) =>
            log.info(s"CreatedEvent failed of $id reason: $requestFailure")
        }
      case Left(requestFailure: RequestFailure) => log.info(s"Search for product failed of  reason: $requestFailure")
    }
    success
  }

  def getProducts = {
    var list: List[Product] = List[Product]()
    client.execute {
      search("products" /"doc26")
    }/*
      .await

    match {
      case Right(rs@RequestSuccess(status, body, headers, result)) =>
        val searchResponse = rs.result
        val searchHit = searchResponse.hits.total
        if (searchHit != 0) {
         for(s <- searchResponse.hits.hits) {
            val doc = s.sourceAsMap
            list = Product(doc("id").asInstanceOf[Int],
              doc("category_id").asInstanceOf[Int],
              doc("name").asInstanceOf[String],
              Some(doc("supplier").asInstanceOf[String]),
              Some(doc("description").asInstanceOf[String])
            ) :: list
          }
        }
      RequestSuccess(status, Some(list.reverse.toString()), headers, result)

      case Left (RequestFailure(status, body, headers, error))=>  RequestFailure(status,Some(list.toString()),headers,error)
    }*/
    //list.reverse
  }

  def agr(date1:String, date2:String) = {
    client.execute {
      search ("sales"/ "sales_type")
        .query {rangeQuery("date") gte date1   lte date2 }
        .aggs { termsAgg("s1","product_name")}.aggs (sumAgg("sums","total_sum"))
    }.await  match{
      case Right(RequestSuccess(status, body, headers, result)) =>  println("*** SUM RESULT : "+result.aggregationsAsMap("sums")  )
        result.aggregationsAsMap("sums").asInstanceOf[Map[String,Any]]("value") .toString
      case Left(requestFailure: RequestFailure) => println ("agr failure")
        requestFailure .toString
    }
  }

  def agr2 = {
    client.execute {
      search ("sales"/ "sales_type")
        .matchQuery("product_name","IphoneX")
        .aggs { sumAgg("sums","total_sum")  }
    }.await  match{
      case Right(RequestSuccess(status, body, headers, result)) =>
        result.aggregationsAsMap("sums").asInstanceOf[Map[String,Any]]("value")
      case Left(requestFailure: RequestFailure) => println ("agr2 failure")
    }
  }

  def updateProduct(dto:DtoProduct) = {
    client.execute {search("products") .bool(must(matchQuery("name",dto.name),matchQuery("category_id",dto.category_id)))
    }
    /*
          .await  match {
          case Right(RequestSuccess(status, body, headers, result)) =>
            println("HITS: "+result.hits.total)
            if (result.hits.total > 0) {
              val id_ = result.hits.hits.head.id
              println("ID : "+id_)
              client.execute {
                update(id_).in("products" / "doc26").docAsUpsert("category_id"-> dto.category_id, "name" -> dto.name,"description"->dto.description.getOrElse(" "),"supplier"->dto.supplier.getOrElse(" "))
              }.await match {
                case Right(p:RequestSuccess[UpdateResponse])  =>  log.info(s"Update product implemented $p")
                  p
                case Left(requestFailure: RequestFailure)  =>  log.info(s"Update product failed of reason: $requestFailure")
                  requestFailure
              }
            }
          case Left(requestFailure: RequestFailure) =>
            println ("Search for UPDATE failed : "+requestFailure)
            requestFailure
        }
        */

  }

}

/*
client.execute {
  search in "index"/ "type" query matchall aggs (
  agg terms "field1_agg" field "field1" aggs (
  agg terms "field2_agg" field "field2" aggs (
  agg sum "agg_bucket" field "integer-field"
  )
  )
  )
}
*/



/*
def createNew = client .execute{
    indexInto("products" / "newtype" ) . fields("f1"->"value1").refresh(RefreshPolicy.WaitFor)

  }

def response = client.execute {
    search("categories").matchQuery("category_id", "NEwSome")
  }.await match {
    case Right(requestSuccess: RequestSuccess[SearchResponse]) => println (requestSuccess.result.hits.hits.head.sourceAsString)
    case Left(requestFailure: RequestFailure) => println ("failure")

  }





 */



















/*
def read(id: String)  ={
  client.execute {
  search("persons" / "person") query idsQuery(id)
}.await match {
  case Right(requestSuccess: RequestSuccess[SearchResponse]) =>
  log.info(s"CreatedEvent implemented ${requestSuccess.result.hits.total}")
  case Left(requestFailure: RequestFailure) =>
  log.info(s"CreatedEvent failed of reason: ${requestFailure}")
}
}

*/

//def searchPayChsById(merchantId: String, invoiceId: String, offset: Int, limit: Int, filter: String): Future[Seq[InvoicePaidCheque]] = {
//    val seq: scala.collection.mutable.ListBuffer[InvoicePaidCheque] = scala.collection.mutable.ListBuffer[InvoicePaidCheque]()
//
//    elasticsearchDAO.read(merchantId,invoiceId, offset, limit, filter) map {
//
//      case Right(requestSuccess: RequestSuccess[SearchResponse]) =>
//        val searchResponse = requestSuccess.result
//        val searchHit = searchResponse.hits.total
//        if (searchHit != 0) {
//          searchResponse.hits.hits.map { s =>
//            val doc = s.sourceAsMap
//            log.info(s"Source: ${ssearchResponse.hits.hits.map { s =>
////            val doc = s.sourceAsMap.sourceAsString}")
//            seq += InvoicePaidCheque(
//              doc("invoiceId").asInstanceOf[String],
//              doc("id").asInstanceOf[String],
//              doc("invoiceType").asInstanceOf[Map[String, Any]]("stringVal").asInstanceOf[String],
//              doc("invoiceStatus").asInstanceOf[String],
//              doc("paymentSystemName").asInstanceOf[String],
//              doc("paymentSystem").asInstanceOf[String],
//              doc("transactionId").asInstanceOf[String],
//              DateTime.parse(doc("transactionTime").asInstanceOf[String]),
//              doc("transactionAccount").asInstanceOf[Map[String, Any]],
//              doc.getOrElse("owner",Map.empty).asInstanceOf[Map[String, Any]].getOrElse("role", "").asInstanceOf[String],
//              doc("amount").asInstanceOf[Double],
//              doc("currency").asInstanceOf[String],
//              doc("payer").asInstanceOf[Map[String, Any]],
//              doc("items").asInstanceOf[Seq[Map[String, Any]]])
//          }
//        }
//        seq
//
//    }
//  }