package kz.darlab.akka.core

import kz.darlab.akka.domain.entities.DAO_.Product
import kz.darlab.akka.domain.entities.DTO_.DtoProduct

object Products {
  var product_id  = 0
  var productList = List(Product(nextId, 1, "Iphone X", Some("Apple"), Some("iphone x 128GB"))
                       , Product(nextId, 1, "Huawei P10", Some("Huawei"), Some("32GB Ram, 12Mp,5.2Inch"))
                       , Product(nextId, 2, "Macbook", Some("Apple"), Some("Macbook Air 2015"))
                        )
  def nextId = {
    product_id +=1
    product_id
  }

  def addProduct(p: DtoProduct)={
      var check =false
      productList.map(e => if (e.name==p.name && e.category_id==p.category_id) check=true )

      if (!check) {
          val new_product =  Product (nextId,p.category_id,p.name,Some(p.supplier.getOrElse("")),Some(p.description.getOrElse("")))
          productList ++= List(new_product)
          true
      }
      else false
  }

  def deleteProduct (dto:DtoProduct)={
      var y = -1
      var check =false
      var productList2: List[Product] = Nil

      productList.map (e=>
          if (e.category_id == dto.category_id && e.name.matches(dto.name))   {
              y = productList.indexOf(e)
              check = true
          })

      if (check) {
          for (i <- 0 to productList.size - 1 if i != y) productList2 = productList(i) :: productList2
          productList = productList2.reverse
          true
      }

      else  {   //println("************** else "+productList)
        false  }

  }

  def getProducts: List[Product] = productList


  def deleteProductById(id_ : BigInt): Unit = {
    var y = -1
    productList.map(e => if (e.id == id_) y = productList.indexOf(e))
    var productList2: List[Product] = Nil

    for (i <- 0 to productList.size - 1 if i != y)  productList2 = productList(i) :: productList2
    productList = productList2.reverse
  }

}
