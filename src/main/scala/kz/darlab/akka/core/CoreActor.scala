package kz.darlab.akka.core

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.sksamuel.elastic4s.http.{RequestFailure, RequestSuccess}
import kz.darlab.akka.domain.Exceptions.DarErrorCodes._
import kz.darlab.akka.domain.amqp._
import kz.darlab.akka.domain.entities.DAO_._
import kz.darlab.akka.utils.DarEndpoint
import kz.darlab.akka.domain.Exceptions.StatusCodes._
import kz.darlab.akka.domain.Exceptions._
import kz.darlab.akka.domain.amqp.Msgs.UserRegistr
import kz.darlab.akka.domain.entities.DTO_.{DtoProduct, DtoWebUser, Dto_Aggregate}


object CoreActor {
  def props(publisher: ActorRef, elasticsearchDAO: ElasticsearchDAO): Props = Props(new CoreActor(publisher,elasticsearchDAO))
  var product_id =1
}

class CoreActor(publisher: ActorRef,elasticsearchDAO: ElasticsearchDAO)  extends  Actor with ActorLogging with DarEndpoint {

  override def receive: Receive = {

    case r@Request(replyTo, headers, body, routingKey) =>

      routingKey match {

        case Msgs.AddProduct.routingKey =>
            val dto = body.asInstanceOf[DtoProduct]
            CoreActor.product_id += 1
            val result = elasticsearchDAO.addProduct(dto)
            if(result)  context.parent ! Accepted(OK.intValue, Some("Product successfully added.."))
            else        context.parent ! ProductAllreadyExistsException(PRODUCT_ALLREADY_EXISTS(errorSeries, errorSystem))

        case Msgs.GetProduct.routingKey =>
            val res = elasticsearchDAO.getProducts
            context.parent !   SeqEntity[Product](res)

        case Msgs.DeleteProduct.routingKey =>
            val result = elasticsearchDAO.delete(body.asInstanceOf[BigInt].intValue())
            if (result)          context.parent ! Accepted (OK.intValue, Some("Product deleted"))
            else                 context.parent ! ProductNotFoundException(PRODUCT_NOT_FOUND(errorSeries,errorSystem))

        case Msgs.UserLogin.routingKey =>
            val result = elasticsearchDAO.userLogin(body.asInstanceOf[DtoWebUser])
            if (result) context.parent ! Accepted(OK.intValue, Some("User logged in.."))
            else        context.parent ! UserNotFoundException(USER_NOT_FOUND(errorSeries, errorSystem))

        case Msgs.UpdateProduct.routingKey =>
            val result = elasticsearchDAO.updateProduct(body.asInstanceOf[DtoProduct])
            context.parent ! Accepted(OK.intValue, Some(result.toString))
          //else context.parent ! UserNotFoundException(USER_NOT_FOUND(errorSeries, errorSystem))

        case Msgs.Aggr.routingKey =>
            val dto = body.asInstanceOf[Dto_Aggregate]
            val res = elasticsearchDAO.agr(dto.date1,dto.date2)
            context.parent ! Accepted(OK.intValue, Some(s"Sum of sales amount of products between dates: ${dto.date1} and ${dto.date1}:  $res"))

        case Msgs.Aggr2.routingKey =>
            val res = elasticsearchDAO.agr2(body.toString )
            context.parent ! Accepted(OK.intValue, Some(s"Sum of sales amount of product ${body.toString} is $res"))

        case Msgs.UserRegistr.routingKey =>
            val result = elasticsearchDAO.regUser(body.asInstanceOf[DtoWebUser])
            if (result) context.parent ! Accepted(OK.intValue, Some(result.toString))
            else        context.parent ! UserAllreadyExistsException(USER_ALLREADY_EXISTS(errorSeries,errorSystem))

        case Msgs.PostTest.routingKey =>

        case _ =>  ()

      }
  }
}




