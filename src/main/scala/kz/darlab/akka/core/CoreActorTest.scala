package kz.darlab.akka.core

import com.sksamuel.elastic4s.http.{RequestFailure, RequestSuccess}
import kz.darlab.akka.domain.Exceptions.DarErrorCodes.INTERNAL_SERVER_ERROR
import kz.darlab.akka.domain.Exceptions.ServerErrorRequestException
import kz.darlab.akka.domain.entities.DAO_.Product
import kz.darlab.akka.domain.entities.DTO_.DtoProduct
import kz.darlab.akka.utils.DarEndpoint

import scala.concurrent.Future
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class CoreActorTest (elastocDAO :ElasticsearchDAOTest) extends DarEndpoint {
  case class Prods ()

  var list: List[Product] = List[Product]()

  def getProdList : Future[List[Product]] = {
    val res = elastocDAO.getProducts
    res map {
      case Right(RequestSuccess(status, body, headers, result)) =>
        val searchResponse = result
        val searchHit = searchResponse.hits.total
        if (searchHit != 0) {
          for (s <- searchResponse.hits.hits) {
            val doc = s.sourceAsMap
            var prod = Product (doc("id").toString.toInt,doc("category_id").toString.toInt,doc("name").toString,Some(doc("supplier").toString),Some(doc("description").toString))

            list = prod :: list
          }
        }
        list.reverse
      case Left(RequestFailure(status, body, headers, error)) =>
        throw new ServerErrorRequestException(INTERNAL_SERVER_ERROR(errorSeries, errorSystem))

    }
  }
  def  updateProduct(dto :DtoProduct) ={

    val res = elastocDAO.updateProduct(dto)
    res map {
      case Left (RequestFailure(status, body, headers, error))  => 1
      //  throw new ServerErrorRequestException(INTERNAL_SERVER_ERROR(errorSeries, errorSystem))

    }
  }
}