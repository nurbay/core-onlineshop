import sbt._

object Version {
  val akka              = "2.5.3"
  val spray             = "1.3.3"
  val json4sVersion     = "3.5.3"
  val scala             = "2.11.7"
  val scalaTest         = "2.2.4"
  val specs2            = "3.3.1"
  val specs2Core        = "4.2.0"
  val specs2Mock        = "3.8.6"

  val typesafeConfig    = "1.3.0"
  val logback           = "1.1.3"
  val jodaTime          = "2.8.2"
  val jodaConverter     = "1.7"
  val emailValidator    = "0.4.0"
  val amqpClient        = "5.0.0"
  val stoneAmqpClient   = "1.5"
  val akkaDomain        = "1.1.1"
  val elastic4sVersion  = "6.0.0"
  val spraySessionVersion ="0.1.0-SNAPSHOT"

}

object Library {
  val akkaActor       = "com.typesafe.akka"    %% "akka-actor"        % Version.akka
  val akkaSlf4j       = "com.typesafe.akka"    %% "akka-slf4j"        % Version.akka
  val sprayCan        = "io.spray"             %% "spray-can"         % Version.spray
  val sprayRouting    = "io.spray"             %% "spray-routing"     % Version.spray
  val json4s          = "org.json4s"           %% "json4s-native"     % Version.json4sVersion
  val typesafeConfig  = "com.typesafe"         %  "config"            % Version.typesafeConfig

  val akkaTestKit     = "com.typesafe.akka"    %% "akka-testkit"      % Version.akka          % "test"
  val sprayTestKit    = "io.spray"             %% "spray-testkit"     % Version.spray         % "test"
  val specs2Core      = "org.specs2"           %% "specs2-core"       % Version.specs2Core    % "test"
  val spec2           = "org.specs2"           %% "specs2"            % Version.specs2        % "test"
  val specs2Mock      = "org.specs2"           %% "specs2-mock"       % Version.specs2Mock    % "test"
  val scalaTest       = "org.scalatest"        %  "scalatest_2.11"    % Version.scalaTest     % "test"

  val logback         = "ch.qos.logback"       %  "logback-classic"   % Version.logback
  val joda            = "joda-time"            %  "joda-time"         % Version.jodaTime
  val jodaConverter   = "org.joda"             %  "joda-convert"      % Version.jodaConverter


  val amqpClient       = "com.rabbitmq"             % "amqp-client"           % Version.amqpClient
  val stoneAmqpClient  = "com.github.sstone"        % "amqp-client_2.11"      % Version.stoneAmqpClient
  val elastic          = "com.sksamuel.elastic4s"   %% "elastic4s-core"       % Version.elastic4sVersion
  val elasticEmbedded  = "com.sksamuel.elastic4s"   %% "elastic4s-embedded"   % Version.elastic4sVersion    % "test"
  val elasticHTTP      = "com.sksamuel.elastic4s"   %% "elastic4s-http"       % Version.elastic4sVersion
  val elasticJson4s    = "com.sksamuel.elastic4s"   %% "elastic4s-json4s"     % Version.elastic4sVersion
  val elasticJackson   = "com.sksamuel.elastic4s"   %% "elastic4s-jackson"    % Version.elastic4sVersion
  val akkaDomain       = "kz.darlab"                %% "domain-onlineshop"    % Version.akkaDomain
  val spraySession = "org.gnieh" %% "spray-session" % Version.spraySessionVersion

}

object Dependencies {

  import Library._
  val depends = Seq(
    akkaActor,
    akkaSlf4j,
    json4s,
    typesafeConfig,
    akkaTestKit,
    scalaTest,
    logback,
    joda,
    jodaConverter,
    sprayCan,
    sprayRouting,
    elastic,
    elasticHTTP,
    elasticJson4s,
    elasticJackson,
    akkaDomain,
    specs2Core,
    specs2Mock,
    "com.rabbitmq"      %   "amqp-client"       % "3.5.3",
    "com.github.sstone" %   "amqp-client_2.11"  % "1.5"
    ,spraySession
  //"org.specs2" %% "specs2-mock" % "4.3.2" % Test
  )
}